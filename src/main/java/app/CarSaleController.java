package app;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class CarSaleController {

    @RequestMapping("/")
    String render() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String render(@RequestBody CarRequest carRequest) {
        Car car = new Car(carRequest.carID, carRequest.vin, carRequest.model);
        return "Added car: " + car.toString();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({IllegalArgumentException.class})
    public void handleException() {
    }
}
