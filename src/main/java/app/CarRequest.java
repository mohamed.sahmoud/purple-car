package app;

public class CarRequest {
    public final Integer carID;
    public final String vin;
    public final String model;

    public CarRequest(Integer carID, String vin, String model) {
        this.carID = carID;
        this.vin = vin;
        this.model = model;
    }
}
