package app;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Car {

    private final ID carID;
    private final VIN vin;
    private final Model model;
    //private String plateNo;
    //private File photo;

    @JsonCreator
    public Car(Integer carID, String vin, String model) {
        this.carID = new ID(carID);
        this.vin = new VIN(vin);
        this.model = new Model(model);
    }

    public ID getcarID() {
        return this.carID;
    }

    public VIN getvin() {
        return this.vin;
    }

    public Model getmodel() {
        return this.model;
    }

    @Override
    public String toString() {
        return this.carID.toString() + " " + this.vin + " " + this.model;
    }

    public static class ID {

        private final int value;

        public ID(Integer value) {
            if (value == null || value <= 0) {
                throw new IllegalArgumentException("Invalid car identity");
            }
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public static class VIN {

        private final String value;

        public VIN(String value) {
            if (value == null || !value.matches("^[A-Z0-9]{17}$")) {
                throw new IllegalArgumentException("Invalid VIN");
            }
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public static class Model {

        private final String value;

        public Model(String value) {
            if (value == null || !value.matches("^[a-zA-Z 0-9]{2,20}$")) {
                throw new IllegalArgumentException("Invalid model");
            }
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
