package app;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Year;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("Usability unit tests")
@WebMvcTest
@Tag("security")
public class appSecuritySpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldFailWithoutContent() throws Exception {
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldThrowExceptionIfCarIDIsNull() throws Exception {
        String content = "{\"vin\": \"123456789ABCSDFEJ\", \"model\": \"foo\"}";
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @ValueSource(ints = {Integer.MIN_VALUE, -1, 0})
    @ParameterizedTest
    public void shouldThrowExceptionIfCarIDIsInvalid(Integer carID) throws Exception {
        String content = String.format("{\"carID\": %d, \"vin\": \"123456789ABCSDFEJ\", \"model\": \"foo\"}", carID);
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldThrowExceptionIfVinIsNull() throws Exception {
        String content = "{\"carID\": 1, \"model\": \"foo\"}";
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @ValueSource(strings = {"", "FFFFFFFFFFFF#F-F", "123", "123456789ASDFGHJKL"})
    @ParameterizedTest
    public void shouldThrowExceptionIfCarVinIsInvalid(String vin) throws Exception {
        String content = String.format("{\"carID\": 1, \"vin\": \"%s\", \"model\": \"foo\"}", vin);
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldThrowExceptionIfModelIsNull() throws Exception {
        String content = "{\"carID\": 1, \"vin\": \"%s\"}";
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void shouldThrowExceptionIfModelIsInTheFuture() throws Exception {
        Car car = new Car(1,"123456789ABCSDFEJ",String.valueOf(Year.now().getValue() + 1));
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @ValueSource(strings = {"", "A", "KJ- 1", "KKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK", "#jK"})
    @ParameterizedTest
    public void shouldThrowExceptionIfCarModelIsInvalid(String model) throws Exception {
        String content = String.format("{\"carID\": 1, \"vin\": \"123456789ABCSDFEJ\", \"model\": \"%s\"}", model);
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }
}
